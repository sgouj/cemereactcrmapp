import React from 'react';
import {  useSelector } from "react-redux";

const CustomerPolicyInfo = ({custDetails, loading, error}) => {

    const  formatDate =(date) => {
        let datetime = date.getDate() + "/"+ parseInt(date.getMonth()+1)  +"/"+ date.getFullYear() + " " +
        date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();

       return datetime;
    }

    const expandCollapseDet = (e) => {
        e.target.classList.toggle("active");
       if(e.target.parentElement)
       {
           if(e.target.parentElement.nextElementSibling.classList.value.includes("paneldet"))
           {
            e.target.parentElement.nextElementSibling.classList.value = "panel";
           }
           else{
            e.target.parentElement.nextElementSibling.classList.value = "paneldet";
           }
       }
    }

    if (error) {
        
		return (
            <div className="alert alert-danger" role="alert">
                {error.message}
                </div>
        );
	}

    if (loading) {
		return (
			<div className="d-flex justify-content-center">
				<div
					className="spinner-border m-5"
					style={{ width: "4rem", height: "4rem" }}
					role="status"
				>
					<span className="sr-only">Loading...</span>
				</div>
			</div>
		);
    }

    if(custDetails.length === 0)
    {
        return (
            <div className="col-md-12 inner">
        <div className="col-md-12 tabsContainer"></div>
            <div className="col-md-12 alert alert-info" role="alert">
                 No Policy Info found.
            </div>
            </div>
           
		);
    }

    return(
        <div className="col-md-12 inner">
        <div className="col-md-12 tabsContainer">
       <br>
       </br>
      

        <h4  className="my-0 font-weight-primary" style={{color: "#007bff"}}>  Policy Details </h4>
        <br></br>
       { custDetails.map((policy) => {
            return(
                <div key={policy.id}>
                    
                    <div className="col-md-12 panelContainer" onClick={expandCollapseDet} style={{ display:"inline-flex",paddingLeft:"0px"}}>
                        <button id="p1" className="accordion" > {policy.policyNumber}   -    {policy.status} </button>
                    </div>   
                    <div className="col-md-12 panel" style={{paddingLeft:"0px"}} id="policydet">
                    <div className="detailbackgroudcolor">
                    <div className="col-md-12 my-1 text-left" style={{ display:"inline-flex"}}>
                            <div className="col-md-3 detailtextfont">
                                Effective Date:
                                </div>
                                <div className="col-md-3">
                                { policy.effectiveDate && formatDate(new Date(policy.effectiveDate))}
                                </div>
                                <div className="col-md-3 detailtextfont">
                                Premium Amount:
                                </div>
                                <div className="col-md-3">
                                {policy.premiumAmount}
                                </div>
                        </div>
                        <div className="col-md-12 my-1 text-left" style={{ display:"inline-flex"}}>
                            <div className="col-md-3 detailtextfont">
                                Product Type:
                                </div>
                                <div className="col-md-3">
                                {policy.productType}
                                </div>
                                <div className="col-md-3 detailtextfont">
                                Company:
                                </div>
                                <div className="col-md-3">
                                {policy.company}
                                </div>
                        </div>
                        <div className="col-md-12 my-1 text-left" style={{ display:"inline-flex"}}>
                        <div className="col-md-3 detailtextfont">
                            Termnated Date:
                            </div>
                            <div className="col-md-3">
                            { policy.terminationDate && formatDate(new Date(policy.terminationDate))}
                            </div>
                            <div className="col-md-3 detailtextfont">
                            Terminated Reason:
                            </div>
                            <div className="col-md-3">
                            {policy.terminationReason}
                            </div>
                        </div>
                    </div>   
                    </div>
               
        </div>
                )}
                )
       }

</div>

</div>
)


    };

export default CustomerPolicyInfo;