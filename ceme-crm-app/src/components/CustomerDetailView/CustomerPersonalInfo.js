import React from 'react';


const CustomerPersonalInfo = ({customer}) =>{
    return(
        <div className="col-md-12" >
                
        <div  className="card-header text-left">
                    <h4  className="my-0 font-weight-primary" style={{color: "#007bff"}}> 
                    Personal Details
                    </h4>
         </div>

<div className="detailbackgroudcolor" style={{border: "1px solid rgb(234 185 125)",margin:"7px"}}>
    <div className="col-md-12 my-1 text-left" style={{ display:"inline-flex"}}>
        <div className="col-md-3 detailtextfont">
            Name:
        </div>
        <div className="col-md-3">
            {customer.lastName}, {customer.firstName}
        </div>
        <div className="col-md-3 detailtextfont">   
            Age:
        </div>
        <div className="col-md-3">
            {customer.age}
        </div>
    </div>
    <div className="col-md-12 my-1 text-left" style={{ display:"inline-flex"}}>
        <div className="col-md-3 detailtextfont">
            Gender:
        </div>
        <div className="col-md-3">
           {customer.gender}
        </div>
        <div className="col-md-3 detailtextfont">
            Language:
        </div>
        <div className="col-md-3">
            {customer.language}
        </div>
    </div>
    <div className="col-md-12 my-1 text-left" style={{ display:"inline-flex"}}>
        <div className="col-md-3 detailtextfont">
            Address:
        </div>
        <div className="col-md-3">
           {customer.address}
        </div>
        <div className="col-md-3 detailtextfont">
            SSN:
        </div>
        <div className="col-md-3">
            {customer.ssn}
        </div>
    </div>
    <div className="col-md-12 my-1 text-left" style={{ display:"inline-flex"}}>
        <div className="col-md-3 detailtextfont">
            Phone:
        </div>
        <div className="col-md-3">
            {customer.phone}
        </div>
        <div className="col-md-3 detailtextfont">
            Email:
        </div>
        <div className="col-md-3">
           {customer.email}
        </div>
    </div>
</div>

</div>
    )
}

export default CustomerPersonalInfo;