import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { searchCustomers } from "../../redux/actions/searchActions";
import queryString from "query-string";
import "./search.css";
import { useLocation } from "react-router-dom";

const SearchForm = (props) => {
  const dispatch = useDispatch();
  const location = useLocation();
  const availableSearchTypes = [
    { name: "Name", value: "name" },
    { name: "Policy Number", value: "policynumber" },
    { name: "Phone Number", value: "phonenumber" },
    { name: "Email", value: "email" },
  ];
  const {
    register,
    handleSubmit,
    errors,
    formState,
    setValue,
    getValues,
  } = useForm({
    mode: "onBlur",
  });

  const previousSearchedText = useSelector(
    (state) => state.search.data.searchText
  );
  const previousSearchedType = useSelector(
    (state) => state.search.data.searchType
  );
  const [isSearchInputDisabled, setSearchInputDisabled] = useState(true);

  useEffect(() => {
    setValue("searchText", previousSearchedText);
    setValue("searchType", previousSearchedType);
    const queryParams = queryString.parse(location.search);
    let searchTypeBy =
      queryParams.searchType && queryParams.searchType.toLowerCase();
    if (
      queryParams.searchText &&
      availableSearchTypes.some((type) => type["value"] === searchTypeBy)
    ) {
      setSearchInputDisabled(false);
      setValue("searchText", queryParams.searchText);
      setValue("searchType", searchTypeBy);
      dispatch(searchCustomers(queryParams.searchText, searchTypeBy));
    }
    if (getValues().searchType !== "") {
      setSearchInputDisabled(false);
    }
  }, [location]);

  const onChangeSearchType = (event) => {
    if (event.currentTarget.value === "") {
      setSearchInputDisabled(true);
    } else {
      setSearchInputDisabled(false);
    }
    setValue("searchText", "");
  };

  const onSubmit = (data, event) => {
    event.preventDefault();
    dispatch(searchCustomers(data.searchText, data.searchType));
  };

  return (
    <React.Fragment>
      <h3>Customer Search</h3>
      <div>
        <p style={{ color: "red" }}>
          {errors.searchText && "Search text is Required"}
        </p>
        <p style={{ color: "red" }}>
          {errors.searchType && "Search type is Required"}
        </p>
      </div>
      <form onSubmit={handleSubmit(onSubmit)} className="form-inline">
        <div>Select Search Type &nbsp;&nbsp;</div>
        <div className="form-group mb-2">
          <select
            name="searchType"
            ref={register({ required: true })}
            id="searchType"
            className="form-control"
            onChange={onChangeSearchType}
            style={{ borderColor: errors.searchType && "red" }}
          >
            <option value="">Choose...</option>
            {availableSearchTypes.map((type) => (
              <option key={type.value} value={type.value}>
                {type.name}
              </option>
            ))}
          </select>
        </div>
        <div className="form-group mx-sm-3 mb-2">
          <input
            className="form-control"
            name="searchText"
            id="searchText"
            ref={register({ required: true })}
            disabled={isSearchInputDisabled}
            style={{
              borderColor:
                errors.searchType && !setSearchInputDisabled && "red",
            }}
          />
        </div>
        <input
          type="submit"
          value="Search"
          className="btn btn-primary mb-2"
          disabled={formState.isSubmitting}
        />
      </form>
    </React.Fragment>
  );
};

export default SearchForm;
