import React from "react";
import { Button } from "@material-ui/core";
import { useHistory } from "react-router-dom";

const PageNotFound = () => {
	const history = useHistory();
	const redirectHome = () => {
		history.push("/");
	};
	return (
		<React.Fragment>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="error-template">
							<h1>Oops!</h1>
							<h2>404 Not Found</h2>
							<div class="error-details">
								Sorry, an error has occured, Requested page not found!
							</div>
							<div class="error-actions">
								<Button
									size="small"
									variant="contained"
									color="primary"
									onClick={() => redirectHome()}
								>
									Take me to home
								</Button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</React.Fragment>
	);
};

export default PageNotFound;
