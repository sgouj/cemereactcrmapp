import custDetailTypes from "./actionTypes";
import axios from "axios";

export const getCustomerDetailBegin = () => {
	console.log("Begin customer detail search");
	return {
		type: custDetailTypes.GET_CUSTOMER_DETAIL_BEGIN,
	};
};

export const getCustomerDetailSuccess = (response) => {
	console.log("getCustomerDetail", response);
	return {
		type: custDetailTypes.GET_CUSTOMER_DETAIL_SUCCESS,
		payload: response ,
	};
};

export const getCustomerDetailFailure = (err) => {
	return {
		type: custDetailTypes.GET_CUSTOMER_DETAIL_FAILURE,
		payload: { message: `Fetching customers details failed` },
	};
};

export const getCustomerDetail = (custId) => {
	console.log("cust id value", custId);
	return (dispatch, getStatus) => {
		dispatch(getCustomerDetailBegin());
		axios
			.get(
				`http://localhost:8080/api/customer/policy/${custId}`
			)
			.then(
				(res) => {
					return dispatch(getCustomerDetailSuccess(res.data));
					
				},
				(err) => {
					 dispatch(getCustomerDetailFailure(err));
				}
			);
	};
};

