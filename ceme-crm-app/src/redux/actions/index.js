import actions from "./actionTypes";

export const getApplicationStatusSuccess = (status) => {
	return {
		type: actions.GET_APPLICATION_STATUS_SUCCESS,
		payload: status,
	};
};

export const getApplicationStatusFailure = (err) => {
	return {
		type: actions.GET_APPLICATION_STATUS_FAILURE,
		payload: { message: "CRM app is down. please try again later" },
	};
};

// to be call by the components
export const getApplicationStatus = () => {
	// returns the thunk function
	return (dispatch, getState) => {
		// Need to call an API
		setTimeout(() => {
			dispatch(getApplicationStatusSuccess("Loading..."));
		});
	};
};
