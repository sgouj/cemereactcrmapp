import custInteractionTypes from "../../redux/actions/actionTypes";

export const CRM_REDUCER_INITIAL_STATE = {
	custInteractions: [],
	loadingInt: false,
	errorInt: false,
};

const customerInteractionReducer = (state = CRM_REDUCER_INITIAL_STATE, action) => {
	switch (action.type) {
		case custInteractionTypes.GET_INTERACTION_DETAIL_BEGIN:
			return {
				...state,
				loadingInt: true,
			};
		case custInteractionTypes.GET_INTERACTION_DETAIL_SUCCESS:
			return {
				...state,
				custInteractions: action.payload,
				loadingInt: false,
			};
		case custInteractionTypes.GET_INTERACTION_DETAIL_FAILURE:
			return {
				...state,
				errorInt: action.payload,
			};
		default: {
			
			return state;
		}
	}
};

export default customerInteractionReducer;
