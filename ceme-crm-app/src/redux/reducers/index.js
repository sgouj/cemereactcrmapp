import actions from "./../actions/actionTypes";
import { combineReducers } from "redux";
import searchReducer from "./searchReducer";
import saveReducer from "./saveReducer";
import customerDetailReducer from "./customerDetailReducer";
import customerInteractionReducer from "./CustIneractionReducer";

const initialState = { status: "" };

const appStatusReducer = (state = initialState, action) => {
	console.log(`received ${action.type} dispatch in Reducer`);
	switch (action.type) {
		case actions.GET_APPLICATION_STATUS_SUCCESS:
			return { ...state, status: action.payload, loading: false };
		case actions.GET_APPLICATION_STATUS_FAILURE:
			return { ...state, status: "", loading: false, error: action.payload };

		default:
			return state;
	}
};

const reducers = combineReducers({
	status: appStatusReducer,
	search: searchReducer,
	save: saveReducer,
	custDetail: customerDetailReducer,
	interactionDetail: customerInteractionReducer,
	
});

export default reducers;
