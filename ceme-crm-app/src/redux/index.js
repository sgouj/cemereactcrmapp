import thunkMiddleware from "redux-thunk";
import { createStore, applyMiddleware } from "redux";

import reducers from "./reducers/index";

const appStore = createStore(reducers, applyMiddleware(thunkMiddleware));

export default appStore;
