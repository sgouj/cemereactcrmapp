import * as index from "../index";

jest.mock("react-dom");

describe("index", () => {
	it("renders without crashing", () => {
		expect(JSON.stringify(index)).toMatchSnapshot();
	});
});
